package models

import (
	"time"

	"gorm.io/gorm"
)

type Person struct {
	Coach     bool
	Firstname string
	Lastname  string
	gorm.Model
}

type Workout struct {
	CoachID        uint
	PersonID       uint
	PersonLastname string
	StartTime      time.Time
	EndTime        time.Time
	gorm.Model
}

// заполненный экземпляр данной структуры будет отдаваться в шаблон на фронтенд для отрисовки
type Page struct {
	Workouts *[]Workout
}

type Msg struct {
	Msg     string
	BackURL string
}
