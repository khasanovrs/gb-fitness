docker:
	sudo rm -rf tmp/db
	docker build . -f build/package/Dockerfile -t gb-fitness
	docker-compose -f deployments/docker-compose.yml up -d --force-recreate

ps:
	docker-compose -f deployments/docker-compose.yml ps

log:
	docker-compose -f deployments/docker-compose.yml logs gb-fitness