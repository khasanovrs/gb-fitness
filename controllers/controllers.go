package controllers

import (
	"fmt"
	"gb-fitness/models"
	"gb-fitness/repository/database"
	"html/template"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

var db database.Database

var Templates struct {
	User          *template.Template
	Coach         *template.Template
	Msg           *template.Template
	SignUpWorkout *template.Template
	CreateWorkout *template.Template
}

func InitTemplates() {
	var err error
	Templates.User, err = template.ParseFiles("views/layout.html", "views/users.html")
	if err != nil {
		log.Fatalf("Init template error: %v", err)
	}
	Templates.Coach, err = template.ParseFiles("views/layout.html", "views/coach.html")
	if err != nil {
		log.Fatalf("Init template error: %v", err)
	}
	Templates.Msg, err = template.ParseFiles("views/msg.html")
	if err != nil {
		log.Fatalf("Init template error: %v", err)
	}
	Templates.SignUpWorkout, err = template.ParseFiles("views/layout.html", "views/sign_up_workout.html")
	if err != nil {
		log.Fatalf("Init template error: %v", err)
	}
	Templates.CreateWorkout, err = template.ParseFiles("views/layout.html", "views/create_workout.html")
	if err != nil {
		log.Fatalf("Init template error: %v", err)
	}
}

func ConnectDB(database database.Database) {
	db = database
}

func MainHandler(w http.ResponseWriter, r *http.Request) {
	workouts := &[]models.Workout{}
	err := db.FindAllWorkouts(workouts)
	if err != nil {
		log.Printf("Get workouts error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
	page := models.Page{
		Workouts: workouts,
	}

	err = Templates.User.ExecuteTemplate(w, "base", page)
	if err != nil {
		log.Printf("Render error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func CoachHandler(w http.ResponseWriter, r *http.Request) {
	workouts := &[]models.Workout{}
	err := db.FindAllWorkouts(workouts)
	if err != nil {
		log.Printf("Get workouts error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
	page := models.Page{
		Workouts: workouts,
	}

	err = Templates.Coach.ExecuteTemplate(w, "base", page)
	if err != nil {
		log.Printf("Render error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func SignUpWorkoutFormHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	workoutIdStr := vars["id"]
	workoutId, err := strconv.Atoi(workoutIdStr)
	if err != nil {
		log.Printf("Parse ID error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}

	var workouts []models.Workout
	workout := &models.Workout{}
	err = db.FindWorkoutByID(workout, workoutId)
	if err != nil {
		log.Printf("Get workout error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
	workouts = append(workouts, *workout)

	page := models.Page{
		Workouts: &workouts,
	}

	err = Templates.SignUpWorkout.ExecuteTemplate(w, "base", page)
	if err != nil {
		log.Printf("Render error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func SignUpWorkoutHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	workoutIdStr := vars["id"]
	workoutId, err := strconv.Atoi(workoutIdStr)
	if err != nil {
		log.Printf("Parse ID error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}

	workout := &models.Workout{}
	err = db.FindWorkoutByID(workout, workoutId)
	if err != nil {
		log.Printf("Get workout error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}

	r.ParseForm()
	name := r.PostFormValue("name")
	lastName := r.PostFormValue("lastname")

	// check that the fields are not empty before sending a request to the database
	if name == "" || lastName == "" {
		err := Templates.Msg.ExecuteTemplate(w, "msg", models.Msg{
			Msg:     "заполнены не все поля",
			BackURL: fmt.Sprint("/workout/", workoutIdStr),
		})
		if err != nil {
			log.Printf("Render error: %v", err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	person := models.Person{
		Coach:     false,
		Firstname: name,
		Lastname:  lastName,
	}

	err = db.CreatePerson(&person)
	if err != nil {
		log.Printf("Create person error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}

	workout.PersonID = person.ID
	workout.PersonLastname = lastName

	err = db.ModifiedWorkout(workout)
	if err != nil {
		log.Printf("Modify workout error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err = Templates.Msg.ExecuteTemplate(w, "msg", models.Msg{
		Msg:     "Вы успешно записались на занятие к тренеру!",
		BackURL: "/",
	})
	if err != nil {
		log.Printf("Render error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func CreateWorkoutFormHandler(w http.ResponseWriter, r *http.Request) {
	err := Templates.CreateWorkout.ExecuteTemplate(w, "base", struct{}{})
	if err != nil {
		log.Printf("Render error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func CreateWorkoutHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	startDateString := r.PostFormValue("start_date")
	startTimeString := r.PostFormValue("start_time")
	endDateString := r.PostFormValue("end_date")
	endTimeString := r.PostFormValue("end_time")

	startTime, err := time.Parse("2006-01-02T15:04:05-0700", fmt.Sprint(startDateString, "T", startTimeString, ":00+0300"))
	if err != nil {
		log.Printf("Time parse error: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	endTime, err := time.Parse("2006-01-02T15:04:05-0700", fmt.Sprint(endDateString, "T", endTimeString, ":00+0300"))
	if err != nil {
		log.Printf("Time parse error: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	workout := &models.Workout{
		StartTime: startTime,
		EndTime:   endTime,
	}

	err = db.CreateWorkout(workout)
	if err != nil {
		log.Printf("Create workout error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err = Templates.Msg.ExecuteTemplate(w, "msg", models.Msg{
		Msg:     "Тренировка создана",
		BackURL: "/trainers",
	})
	if err != nil {
		log.Printf("Render error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func DeleteWorkoutHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	workoutIdStr := vars["id"]
	workoutId, err := strconv.Atoi(workoutIdStr)
	if err != nil {
		log.Printf("Parse ID error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}

	workout := &models.Workout{}
	err = db.FindWorkoutByID(workout, workoutId)
	if err != nil {
		log.Printf("Get workout error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}

	err = db.DeleteWorkoutByID(workout, workoutId)
	if err != nil {
		log.Printf("Delete workout error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err = Templates.Msg.ExecuteTemplate(w, "msg", models.Msg{
		Msg:     "Тренировка усешно удалена!",
		BackURL: "/trainers",
	})
	if err != nil {
		log.Printf("Render error: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}
